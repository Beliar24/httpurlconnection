package ua.ithillel.java.elementary.socket.http.client;


import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimpleHttpClientImpl implements SimpleHttpClient{
    @Override
    public SimpleHttpResponse request(HttpMethod method, String url, String payload, Map<String, String> headers) {
        SimpleHttpResponse response = null;
                try {
                    URL url1 = new URL(url);

                    Map<String, String> maps = new HashMap<>();


                    HttpURLConnection connection = (HttpURLConnection) url1.openConnection();

                    if (method.getMethodName().equals("GET")) {
                        connection.setDoOutput(true);
                        connection.setRequestMethod(method.getMethodName());

                        for (Map.Entry<String, String> stringStringEntry : headers.entrySet()) {
                            connection.setRequestProperty(stringStringEntry.getKey(), stringStringEntry.getValue());
                        }

                        for (Map.Entry<String, List<String>> stringStringEntry : connection.getHeaderFields().entrySet()) {
                            maps.put(stringStringEntry.getKey(), stringStringEntry.getValue().get(0));
                        }
                    } else if (method.getMethodName().equals("POST")) {

                        connection.setRequestMethod("POST");
                        connection.setRequestProperty("Content-Length", Integer.toString(payload.length()));


                        connection.setDoOutput(true);
                        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                        wr.writeBytes(payload);
                        wr.flush();
                        wr.close();

                        System.out.println(payload);

                        for (Map.Entry<String, List<String>> stringStringEntry : connection.getHeaderFields().entrySet()) {
                            maps.put(stringStringEntry.getKey(), stringStringEntry.getValue().get(0));
                        }


                    }

                    response = new SimpleHttpResponse(connection.getResponseCode(), connection.getResponseMessage(),
                            connection.getContentType(), maps);


                } catch (Exception e) {
                    e.printStackTrace();
                }
        return response;
    }

}
