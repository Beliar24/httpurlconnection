package ua.ithillel.java.elementary.socket.http.client;

import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.jupiter.api.Assertions.*;

@WireMockTest(httpPort = 1234)
class SimpleHttpClientImplTest {

    @Test
    void shouldMakeGetRequest() {
        stubTest1();
        SimpleHttpClient client = new SimpleHttpClientImpl();
        SimpleHttpResponse resp = client.get("http://127.0.0.1:1234/get-request-1", Map.of("x-test-request-header","hv"));
        System.out.println(resp);
        assertEquals(200,resp.getStatusCode());
        assertEquals("OK",resp.getStatusText());
        assertEquals("header-value-1",resp.getHeaders().get("x-test-header-1"));
        assertEquals("header-value-2",resp.getHeaders().get("x-test-header-2"));
    }

    @Test
    void shouldMakePostRequest() {
        stubTest2();
        SimpleHttpClient client = new SimpleHttpClientImpl();
        SimpleHttpResponse resp = client.post(
                "http://127.0.0.1:1234/post-request-1",
                "testbody" ,
                Map.of("Content-Type","text/plain")
        );
        System.out.println(resp);
        assertEquals(200,resp.getStatusCode());
        assertEquals("OK",resp.getStatusText());
    }

    void stubTest1() {
        stubFor(get("/get-request-1")
                .withHeader("x-test-request-header",equalTo("hv"))
                .willReturn(
                        ok("hello world")
                                .withHeader("x-test-header-1","header-value-1")
                                .withHeader("x-test-header-2","header-value-2")

                ));
    }

    void stubTest2() {
        stubFor(post("/post-request-1")
                .withRequestBody(equalTo("testbody"))
                .willReturn(
                        ok("hello world")
                ));
    }
}